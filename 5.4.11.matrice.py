print("Affichage avec la Boucle for")
matrice = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
]
taille = 3
for i in range(taille):
    for j in range(taille):
        print(matrice[i][j], end = " ")
    print()
print("Affichage avec la boucle while")
k = 0
l = 0
while k < taille :
    while l< taille :
        print(matrice[k][l], end=" " )
        l=l+1
    print()
    k+=1
    l=0